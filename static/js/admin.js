var last_loaded_time = "never";
var poll_url = '/admin/' + window.location.pathname.split('/')[2] + '/survey-responses/';
var error_block;

$(document).ready(function(){
   error_block = $('#error_block');
   poll();
   setInterval(function(){
       $('.fresh').removeClass('fresh');
   }, 1000);
});

function poll(){

    $.ajax({
        url: poll_url,
        timeout: 60000, //milliseconds
        data: {'last_loaded_time':last_loaded_time},

        success: function(data){
            error_block.hide();
            last_loaded_time = data['last_loaded_time'];
            display_survey_data(data['surveys']);
            display_summary_data(data['summary']);
            setTimeout(poll(), 2000); // give dev server a break
        },

        error: function(response, textStatus, error){
            if(response.status == 404){
                // 404 - no new data from server
                error_block.hide();
                setTimeout(poll(), 2000); // give dev server a break
            } else {
                error_block.show();
            }
        }
    })
}

function display_survey_data(data){
    // data rows arrive 'oldest' first
    var table = $("#survey-data");
    for(var i=0; i < data.length; i++){
        var html = build_survey_template(data[i]['data']);
        var existing = $('#survey-data').children('tr#' + data[i]['row_id']);
        if(existing.length == 1){
            // replace it
            existing.html(html);
            existing.addClass('fresh');
        } else {
            //prepend to table
            html = '<tr class="fresh" id="' + data[i]['row_id'] + '" >' + html + '</tr>';
            var table_html = table.html();
            table.html(html + table_html);
        }
    }


}

function build_survey_template(data){

    var html = '';
    for(var i=0; i < data.length; i++){
        var value = data[i];
        if(value === 0){value = ''}

        switch (true){
            case value === true:
                html += "<td><i class='fa fa-check'></i></td>";
                break;
            case value === false:
                html += "<td><i class='fa fa-close'></i></td>";
                break;
            case $.isArray(value):
                html += "<td>";
                for(var c=0; c < value.length; c++){
                    html += value[c] + "<br>"
                }
                html += "</td>";
                break;
            default:
                html += "<td>" + value + "</td>"
        }
    }

    return html
}

function display_summary_data(data){

    $('.summary').each(function(index){
        var text = data[index];
        if($.isArray(text)){
            var html = '', i = 0, subtext = '';
            while(i < text.length){
                subtext = text[i];
                i++;
                html += "<div>" + i + ". " + subtext + "</div>";
            }
            text = html;
        }
        $(this).children('.summary_data').html(text)
    })
}