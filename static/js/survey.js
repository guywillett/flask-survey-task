
var survey = {
    path: window.location.pathname,
    steps: $('.step'),
    form: $('#survey_form'),
    id_field: $('#id_survey_response_id'),
    complete_field: $('#id_complete'),
    validate_step_field: $('#id_validate_step'),
    question_fields: $('#survey_form').find(':input'),
    completed_block: $('#completed_block'),
    thankyou_block: $('#thankyou_block'),
    error_block: $('#error_block'),
    error_text: $('#survey_errors'),
    loading: $('#loading'),
    survey_buttons: $('#survey_buttons'),
    next_button: $('#next'),
    prev_button: $('#prev'),

    id: false,
    base_url: '/survey/' + window.location.pathname.split('/')[2] + '/survey-response/',
    last_serialized_value: '',

    getStorage: function(key){
        var value = localStorage.getItem(key);
        if (value){
            value = value.replace(this.path, '');
        }
        return value
    },

    setStorage: function(key, value){
        var v = this.path + value;
        localStorage.setItem(key, v)
    },

    show_step: function(step){
        this.steps.hide();
        this.setStorage('survey_step', step);
        switch (true){
            case this.steps.length === 1:
                this.prev_button.hide();
                this.next_button.hide();
                break;
            case step === 1 || step === '1':
                this.prev_button.hide();
                this.next_button.show();
                break;
            case step === this.steps.length || step === this.steps.length.toString():
                this.prev_button.show();
                this.next_button.hide();
                break;
            default:
                this.next_button.show();
                this.prev_button.show();
        }

        $('#step' + step).show();
    },

    change_step: function(direction){
        var step = parseInt(this.getStorage('survey_step'));

        this.error_block.hide();
        var data = this.validate_step(step, false);
        if(data){

            if(direction == 'next'){
                step ++;
            } else {
                step --;
            }
            if (step < 1){step = 1}
            else if(step > this.steps.length){step = this.steps.length}

            if(data['check_data'] != this.last_serialized_value){ // test if data has changed before sending to server
                this.last_serialized_value = data['check_data'];
                var that = this;
                $.ajax({
                    url: this.base_url + this.id + '/',
                    method: 'PUT',
                    data: data['form_data'],
                    success: function(response){
                        if(response['status'] == 'completed'){
                            that.show_step(step);
                        } else {
                             // display server validation errors {'errors': ['message', 'message]}
                            var errors = response['errors'];
                            var message = '';
                            for(var i = 0; i < errors.length; i++){
                                message += errors[i] + '<br>'
                            }
                            that.error_text.html(message);
                            that.error_block.show()
                        }
                    },
                    error: function(){
                        // 5xx error
                        // user doesn't care if data saved...
                        that.show_step(step)
                    }
                });
            } else {
                this.show_step(step);
            }

        }
    },

    finish: function(){
        this.error_block.hide();
        var step = parseInt(this.getStorage('survey_step'));
        var data = this.validate_step(step, true);
        var that = this;
        if(data){
            $.ajax({
                    url: this.base_url + this.id + '/',
                    method: 'PUT',
                    data: data['form_data'],
                    success: function(response){
                        if(response['status'] === 'completed'){
                            // survey is finished!
                            that.steps.hide();
                            that.error_block.hide();
                            that.survey_buttons.hide();
                            that.thankyou_block.show();
                            that.setStorage('survey_completed', 'yes');
                        } else {
                            // display validation errors {'errors': ['message', 'message]}
                            var errors = response['errors'];
                            var message = '';
                            for(var i = 0; i < errors.length; i++){
                                message += errors[i] + '<br>'
                            }
                            that.error_text.html(message);
                            that.error_block.show()
                        }
                    },
                    error: function(response){
                        // connection error or similar (5xx)
                        that.error_text.text("Unable to save your Survey.  Please try again.");
                        that.error_block.show()
                    }

                });

        }
    },

    validate_step: function(step, finishing){
        var valid = true;
        this.complete_field.val('no');
        this.validate_step_field.val(step);

        for(var i=0; i<this.question_fields.length; i++){
            var field = this.question_fields[i];
            if(field.required && ! $(field).val() && $(field).closest('.step').data('step') == step){
                // .val() won't work for radio or checkbox
                valid = false;
                // add error class to failed questions in this step
                $(field).closest('.form-group').addClass('has-error');
                $(field).change(function(e){
                    // won't work for radio or checkbox - no 'e'
                    var el = $(e.target);
                    if(el.val()){
                        el.closest('.form-group').removeClass('has-error')
                    }
                });
            }
        }
        if(valid){
            if(finishing){
                this.complete_field.val('yes');
                this.validate_step_field.val('');
            }
            var data = {
                form_data: this.form.serialize(),
                check_data: $(this.form[0].elements).not("#id_validate_step").serialize()
            };
            return data
        }

        this.error_text.text("Please complete the required fields.");
        this.error_block.show();
        return false
    },

    init: function(){
        /*
        Survey is started if survey_response_id in localStorage
        Make sure a survey_step is in localStorage
        Survey has been completed if survey_completed in localStorage
        we use this.set/getStorage() to ensure path namespacing
        (a user can do more than 1 survey type, but only 1 response per survey type)
         */

        if(this.getStorage('survey_completed') == 'yes'){
            this.loading.slideUp();
            this.completed_block.show();
            return
        }
        if (! this.getStorage('survey_step')){
            this.setStorage('survey_step', '1')
        }
        var that = this; // we lose 'this' inside .post and .get
        this.id = this.getStorage('survey_response_id');

        if(! this.id){
            $.post(this.base_url, function(data){
                that.setStorage('survey_response_id', data['survey_response_id']);
                that.id = data['survey_response_id'];
                that.show_step(that.getStorage('survey_step'));
                that.survey_buttons.show();
            }).fail(function(response, textStatus, error){
                    that.error_text.text("Unable to create your Survey. (status: " + response.status + ")");
                    that.error_block.show();
                });

        } else {
            $.get(this.base_url + this.id + '/', function(data){
                if(data['id'].toString() === that.id){
                    // update form fields to server values
                    for(var i=0; i < that.question_fields.length; i++){
                        var _field = that.question_fields[i];
                        _field = $(_field);
                        var key = _field.attr('name');
                        var type = _field.attr('type');
                        if(data.hasOwnProperty(key)){
                            if(type == 'radio' || type == 'select'){
                                // checkbox data already an Array
                                _field.val([data[key]]);
                            } else {
                                _field.val(data[key]);
                            }
                        }

                    }
                    that.show_step(that.getStorage('survey_step'));
                    that.survey_buttons.show();
                } else {
                    //ERROR - !!! incorrect survey response id, so ignore it.
                }
            }).fail(function(response, textStatus, error){
                    that.error_text.text("Unable to load your previous Survey answers, please try again later. (status: " + response.status + ")");
                    that.error_block.show();
                });
        }

        this.loading.slideUp();

    }
};

$(document).ready(function(){
    survey.init();

    $('#next').click(function(){
        survey.change_step('next')
    });

    $('#prev').click(function(){
        survey.change_step('prev')
    });

    $('#finish').click(function(){
        survey.finish()
    });
});