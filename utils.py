import string
import random


def random_string(length=50):
    chars = list(string.ascii_letters + string.digits)
    char_list = [random.SystemRandom().choice(chars) for i in range(length)]
    return "".join(char_list)