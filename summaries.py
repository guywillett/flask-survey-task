from flask import render_template


class BaseSummary(object):

    def __init__(self, field_object):
        self.field = field_object
        self.template = "summaries/default.html"
        self.label = ""

    def get_summary(self):
        pass

    def admin_html(self):
        return render_template(self.template, field_label=self.field.label, label=self.label)


class AverageSummary(BaseSummary):

    def __init__(self, *args, **kwargs):
        super(AverageSummary, self).__init__(*args, **kwargs)
        self.label = "Average"

    def get_summary(self):
        data = self.field.summary_data
        # average value of 0 is meaningless and don't want 0 division issues
        average = "%.1f" % (float(data['total'])/data['num_rows']) if data['total'] else ""
        return average


class RatioSummary(BaseSummary):

    def __init__(self, *args, **kwargs):
        super(RatioSummary, self).__init__(*args, **kwargs)
        self.label = "Ratio"
        self.template = "summaries/ratio.html"

    def get_summary(self):
        data = self.field.summary_data
        ratio = ''
        ratio_label = ''
        choices = self.field.choices
        ordered_values = data['ordered_values']
        if len(choices) == 2:
            if len(ordered_values) == 2:
                value1 = ordered_values[0][1]
                value2 = ordered_values[1][1]
            elif len(ordered_values) == 1:
                value1 = ordered_values[0][1]
                value2 = 1
            else:
                value1 = value2 = 1

            ratio = float(value1)/value2
            if len(ordered_values) and choices[0][1] == ordered_values[0][0]:
                ratio_label = choices[0][0] + ":" + choices[1][0] + "<br>"
            else:
                ratio_label = choices[1][0] + ":" + choices[0][0] + "<br>"

        return ratio_label + str(ratio)


class Top3Summary(BaseSummary):

    def __init__(self, *args, **kwargs):
        super(Top3Summary, self).__init__(*args, **kwargs)
        self.label = "Top 3"

    def get_summary(self):
        data = self.field.summary_data
        ordered_values = data['ordered_values']
        number_of_values = min(3, len(ordered_values))  # maybe everyone chose the same value!
        top3 = [x[0] for x in ordered_values[:number_of_values]]

        return top3 if len(top3) else ''


def DataPrep(field_name, rows):
    # return a dict that can be consumed by all/any summary class.
    # it's called by each Survey.field that is summarised
    data = {
        'num_rows': len(rows),
        'total': 0,
        'ordered_values': []
    }

    value_dict = {}
    for row in rows:
        value = row[field_name]
        if value:
            if type(value) in [int, float]:
                data['total'] += value
            if type(value) in [list, tuple]:
                for v in value:
                    if v in value_dict:
                        value_dict[v] += 1
                    else:
                        value_dict[v] = 1
            else:
                if value in value_dict:
                    value_dict[value] += 1
                else:
                    value_dict[value] = 1
    if value_dict:
        tuple_list = [(k, v) for k, v in value_dict.iteritems()]
        ordered_values = sorted(tuple_list, key=lambda x: x[1], reverse=True)
        data['ordered_values'] = ordered_values
    return data