from flask import Flask, request, jsonify, abort, make_response
from datetime import datetime, timedelta
import time
from utils import random_string
from sqlalchemy.exc import IntegrityError

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://guy:@localhost/task2"
app.debug = True

from survey import Survey
from forms import ResponseForm


@app.route('/survey/<int:survey_id>/', methods=['GET'])
def survey_form(survey_id):
    survey = Survey.query.get(survey_id)
    if survey:
        return survey.html()
    abort(404)


@app.route('/survey/<int:survey_id>/survey-response/', methods=['POST'])
def create_survey_response(survey_id):
    if request.is_xhr:
        user_id = request.cookies.get('user_id') or random_string()
        expires = datetime.now() + timedelta(days=365*30)
        survey = Survey.query.get(survey_id)
        if survey:
            try:
                survey_response_id = survey.create_response(user_id=user_id)
                response = make_response(jsonify({'survey_response_id': survey_response_id}))
                response.set_cookie('user_id', value=user_id, expires=expires)
                return response
            except IntegrityError:
                abort(403)
        abort(404)
    abort(403)


@app.route('/survey/<int:survey_id>/survey-response/<int:survey_response_id>/', methods=['GET', 'PUT'])
def survey_response(survey_id, survey_response_id):
    if request.is_xhr:
        survey = Survey.query.get(survey_id)
        if survey:
            sr = survey.get_one(survey_response_id)
            if sr and request.cookies.get('user_id') == sr.user_id:

                if request.method == "GET":
                    return jsonify(sr.get_modified_data())

                elif request.method == "PUT" and not sr.completed:
                    data = {'status': 'completed'}
                    form = ResponseForm(sr, request.form)
                    if form.is_valid():
                        form.save()
                    else:
                        data = {
                            'status': 'incomplete',
                            'errors': ["{}: {}".format(k, v) for k, v in form.errors.iteritems()]
                        }
                    return jsonify(data)

                else:
                    abort(403)
        abort(404)
    abort(403)


@app.route('/admin/<int:survey_id>/', methods=['GET'])
def admin(survey_id):
    # should require log-in
    survey = Survey.query.get(survey_id)
    if survey:
        return survey.admin_html()
    abort(404)


@app.route('/admin/<int:survey_id>/survey-responses/', methods=['GET'])
def admin_survey_responses(survey_id):
    # should require log-in
    if request.is_xhr:
        last_loaded_time = request.args.get('last_loaded_time')
        try:
            start = datetime.strptime(last_loaded_time, '%Y-%m-%d.%H.%M.%S.%f')
        except ValueError:
            start = None
        survey = Survey.query.get(survey_id)
        if survey:
            for x in range(30):  # just do 30 cycles so process will die, probably a little before the ajax request
                data, new_start = survey.get_all(start)
                new_start = datetime.strftime(new_start, '%Y-%m-%d.%H.%M.%S.%f')
                if data:
                    summary = survey.get_summary()
                    result = {
                        'surveys': data,
                        'summary': summary,
                        'last_loaded_time': new_start,
                    }
                    return jsonify(result)
                else:
                    time.sleep(1)
        abort(404)
    abort(403)


if __name__ == '__main__':
    app.run(threaded=True)
    # should do error logging in production