from flask import render_template


class BaseField(object):

    def __init__(self, name, required=False, label=None, choices=None):
        self.name = name
        self.required = required
        self.error = ""
        self.data = None
        self.template = ""
        self.summary_data = {}  # used by summaries
        self.choices = choices  # List of tuples (label, value)
        self.allow_multiple_answers = False
        self.label = label or name.title()

    def is_valid(self, data):
        # expecting data as None or a List
        self.data = data
        self.modify_data()
        if self.required and not self.data:
            self.error = "This field is required!"
            return False
        if self.data:
            # validate only if there is data. Otherwise would fail, even if field is not required.
            return self.validate()
        return True

    def validate(self):
        return True

    def modify_data(self):
        # modifies form data before saving to db
        # default values should be falsey
        if self.data:
            # will be a list
            if type(self.data) == list:
                self.data = self.data if self.allow_multiple_answers else self.data[0]
            else:
                raise TypeError('Form data should be a List or falsey')
        else:
            if self.allow_multiple_answers:
                self.data = []
            else:
                # '' translates better than None or null in front end (via json)
                self.data = ""

    def html(self):
        return render_template(self.template, name=self.name, label=self.label, required=self.required, choices=self.choices )


class StringField(BaseField):

    def __init__(self, *args, **kwargs):
        super(StringField, self).__init__(*args, **kwargs)
        self.template = "fields/text_field.html"


class EmailField(BaseField):

    def __init__(self, *args, **kwargs):
        super(EmailField, self).__init__(*args, **kwargs)
        self.template = "fields/email.html"

    def validate(self):
        if '@' in self.data and '.' in self.data:
            return True
        self.error = "Incorrect email format."
        return False


class TextField(BaseField):

    def __init__(self, *args, **kwargs):
        super(TextField, self).__init__(*args, **kwargs)
        self.template = "fields/text_area.html"


class AgeField(BaseField):

    def __init__(self, *args, **kwargs):
        super(AgeField, self).__init__(*args, **kwargs)
        self.template = "fields/age.html"
        if not self.choices:
            self.choices = (18, 81)  # a tuple(start, finish) to use in range(start, finish)

    def modify_data(self):
        super(AgeField, self).modify_data()
        if self.data:
            self.data = int(self.data)
        else:
            self.data = 0


class RadioField(BaseField):

    def __init__(self, *args, **kwargs):
        super(RadioField, self).__init__(*args, **kwargs)
        self.template = "fields/radio.html"
        if not self.choices:
            raise ValueError("'Choices' eg: [(label, value), (label, value)] are required for RadioField")

    def validate(self):
        permitted_values = [choice[1] for choice in self.choices]
        if self.data in permitted_values:
            return True
        else:
            self.error = "Invalid answer"
            return False


class CheckboxField(BaseField):

    def __init__(self, *args, **kwargs):
        super(CheckboxField, self).__init__( *args, **kwargs)
        self.template = "fields/checkbox.html"
        self.allow_multiple_answers = True
        if not self.choices:
            raise ValueError("'Choices' eg: [(label, value), (label, value)] are required for CheckboxField")

    def validate(self):
        permitted_values = [choice[1] for choice in self.choices]
        for answer in self.data:
            if answer not in permitted_values:
                self.error = "Invalid answer"
                return False
        return True