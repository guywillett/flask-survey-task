from survey import db
from datetime import datetime


class ResponseForm(object):

    def __init__(self, survey_response_object, form_data):
        self.survey_response = survey_response_object
        self.form_data = form_data
        self.errors = {}
        self.validated_data = {}

    def is_valid(self):
        # data is modified as well as checked for validity
        step = self.form_data.get('validate_step')
        if step:
            # just validate fields in the step
            step_index = int(step) - 1
            fields = [self.survey_response.survey.fields[i] for i in self.survey_response.survey.steps[step_index]]
        else:
            fields = self.survey_response.survey.fields

        for field in fields:
            field_data = self.form_data.getlist(field.name)  # all fields expect a list

            if field.is_valid(field_data):
                self.validated_data[field.name] = field.data
            else:
                self.errors[field.name] = field.error

        if self.form_data.get("complete") == "yes" and not self.errors:
            self.validated_data['completed'] = True
        else:
            self.validated_data['completed'] = False

        return not self.errors

    def save(self):
        # must use after is_valid()
        if self.validated_data and not self.errors:
            data_changed = False
            old_data = self.survey_response.get_modified_data()

            for k, v in self.validated_data.iteritems():
                if old_data[k] != v:
                    data_changed = True

            self.validated_data['answers'] = {}
            for field in self.survey_response.survey.fields:
                if field.name in self.validated_data:
                    self.validated_data['answers'][field.name] = self.validated_data[field.name]
                    del self.validated_data[field.name]
                else:
                    self.validated_data['answers'][field.name] = old_data[field.name]

            for k, v in self.validated_data.iteritems():
                setattr(self.survey_response, k, v)

            if data_changed:
                self.survey_response.last_modified = datetime.now()
            db.session.commit()
        else:
            raise ValueError('Either no valid data or there are errors')