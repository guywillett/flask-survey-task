from __main__ import app
from flask.ext.sqlalchemy import SQLAlchemy
from flask import render_template
from sqlalchemy.orm import validates, reconstructor
from datetime import datetime
import json
from fields import StringField, TextField, AgeField, RadioField, CheckboxField, EmailField
from summaries import AverageSummary, RatioSummary, Top3Summary, DataPrep

db = SQLAlchemy(app)


class SurveyResponse(db.Model):
    __tableName__ = "survey_response"
    id = db.Column(db.Integer, primary_key=True)
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    user_id = db.Column(db.String, nullable=False)
    answers = db.Column(db.Text)
    completed = db.Column(db.Boolean, default=False)
    last_modified = db.Column(db.DateTime)
    __table_args__ = (db.UniqueConstraint('survey_id', 'user_id', name='survey_user_uc'),)

    def __repr__(self):
        return "Survey response %s" % self.id

    @validates('answers')
    def process_answers(self, key, answers):
        if self.survey:
            for field in self.survey.fields:
                if field.name not in answers:
                    raise ValueError('Answers must contain a key for each Survey field')
        return json.dumps(answers)

    def get_modified_data(self, data_as_list=False):
        # return a dict of field name:value with modified values per field type
        data = {k: getattr(self, k) for k in self.__table__.columns.keys()}
        answers = json.loads(data['answers'])
        for k, v in answers.iteritems():
            data[k] = v
        del data['answers']

        if data_as_list:
            data = self._data_as_list(data)
        return data

    def _data_as_list(self, data):
        # format data for Admin front end
        # return a list of values in field order, along with a 'row_id'
        data_list = [data[field.name] for field in self.survey.fields]
        data_list.append(data['completed'])
        return {'row_id': data['id'], 'data': data_list}


class Survey(db.Model):

    __tableName__ = "survey"
    id = db.Column(db.Integer, primary_key=True)
    responses = db.relationship('SurveyResponse', backref='survey', lazy='dynamic')
    # could have fields like 'name'
    # also fields to relate to Hotjar user

    def __repr__(self):
        return "Survey #%s" % self.id

    @reconstructor
    def init_on_load(self):
        self.__init__()

    def __init__(self, **kwargs):
        super(Survey, self).__init__(**kwargs)
        # Order of fields is important - they will show in this order on Survey form and Admin area.
        # Data passed to front end is 'order' important (it is ignorant of field names).

        self.fields = [
            StringField("name", required=True),
            EmailField("email", required=True),
            AgeField("age", required=True),
            TextField("about", required=True),
            StringField("address"),
            RadioField("gender", choices=[("Male", "male"), ("Female", "female")]),
            StringField("book", label="Favorite Book"),
            CheckboxField("colors", label="Favorite Colors", choices=[
                ("Red", "red"), ("Yellow", "yellow"), ("Green", "green"), ("Purple", "purple"), ("Blue", "blue")
            ])
        ]
        self.steps = [  # A list of steps, each containing a list of self.field indices.
                        # All fields must be included, but only once.
                        # There must be at least one step.
            [0, 1],
            [2, 3],
            [4, 5],
            [6, 7]
        ]

        self.summaries = [  # A list of summaries, each being a list of self.field index and a Summary Class.
                            # Optional - can be an empty list
                            # Order is important
            [2, AverageSummary],
            [5, RatioSummary],
            [7, Top3Summary]
        ]

    def create_response(self, user_id):
        answers = {}
        for field in self.fields:
            field.modify_data()
            answers[field.name] = field.data
        survey_response = SurveyResponse(user_id=user_id, survey_id=self.id, last_modified=datetime.now(), answers=answers)
        db.session.add(survey_response)
        db.session.commit()
        return survey_response.id

    def get_one(self, response_id):
        #  returns a SurveyResponse object or None
        return self.responses.filter_by(id=int(response_id)).first()

    def get_all(self, last_modified=None):
        # return data for all SurveyResponses or those since 'last_modified'
        new_last_modified = last_modified or datetime.now()
        if last_modified:
            rows = self.responses.filter(SurveyResponse.last_modified > last_modified).order_by(SurveyResponse.last_modified).all()
        else:
            rows = self.responses.order_by(SurveyResponse.last_modified).all()
        if len(rows):
            data = [row.get_modified_data(data_as_list=True) for row in rows]
            last_row = rows[len(rows) - 1]  # most recently modified row
            new_last_modified = last_row.last_modified
        else:
            data = None
        return data, new_last_modified

    def get_summary(self):
        result = []
        if self.summaries:
            # could use db avg for age average but need to iterate rows for top_colors anyhow.
            rows = self.responses.all()
            rows = [row.get_modified_data() for row in rows]

            # get a list of self.field indices that are summarised
            fields_indices = set([s[0] for s in self.summaries])  # could be more than 1 summary per self.field

            for index in fields_indices:
                field = self.fields[index]
                # get the 'prepared data' for each summarised self.field
                # this data can be consumed by any/all summaries
                # (it could be cached if necessary) #
                field.summary_data = DataPrep(field.name, rows)

            for s in self.summaries:
                summary = s[1](self.fields[s[0]])  # instantiate the Summary class with the corresponding field object
                summary_result = summary.get_summary()  # get the summary result
                result.append(summary_result)

        return result

    def html(self):
        fields = [f.html() for f in self.fields]
        return render_template("survey.html", fields=fields, steps=self.steps, survey_id=str(self.id))

    def admin_html(self):
        summaries = []
        for summary_list in self.summaries:
            summary = summary_list[1](self.fields[summary_list[0]])  # instantiate the Summary class with the corresponding field object
            summaries.append(summary.admin_html())
        return render_template("admin.html", fields = self.fields, survey_id=str(self.id), summaries=summaries)